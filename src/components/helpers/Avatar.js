import React from 'react';

function Avatar(props) {
    return (
        <div className="avatarDIV flex flex-vertCntr">
            <img className="avatar" src={props.src} alt="Avatar"/>
        </div>
    )
}

export default Avatar
