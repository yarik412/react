import React, { Component } from "react";
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import Spiner from "../animatedElem/Spiner";

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomInform: {
        roomName: "My chat",
        numberUsers: 0,
        numberMessages: 0,
        lastMessageDate: "0",
      },
      messages: [],
      myInform: {
        userId: "61325",
        avatar:
          "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng",
        user: "Wendy",
      },
      edtMsg: {
        id: "",
        editMsg: false,
        text: "",
      },
    };
  }

  componentDidMount() {
    fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          messages: data,
        });
        this.changeHeader();
      });
  }

  changeHeader = () => {
    const allUserId = this.state.messages.map((msg) => {
      return msg.userId;
    });

    const messagesLength = this.state.messages.length;
    const users = new Set(allUserId);

    const createdAt = new Date(
      this.state.messages[messagesLength - 1].createdAt
    ).toLocaleDateString().split('.');
    const today = new Date().toLocaleDateString().split('.');

    let lastMsgTime = createdAt.join('.').slice(0,5);
    if (createdAt[2] !== today[2]){
      lastMsgTime = createdAt.join('.');
    }

    this.setState({
      roomInform: {
        roomName: "My chat",
        numberMessages: messagesLength,
        numberUsers: users.size,
        lastMessageDate: lastMsgTime
      },
    });
  };

  addNewMessage = (data) => {
    const myInform = this.state.myInform;
    const message = {
      id: data.id,
      userId: myInform.userId,
      avatar: myInform.avatar,
      user: myInform.user,
      text: data.text,
      createdAt: data.createdAt,
      editedAt: "",
    };
    this.setState({
      messages: [...this.state.messages, message],
    });
    setTimeout(() => {
      this.changeHeader();
    }, 0);
  };

  deleteMessage = (id) => {
    this.setState({
      messages: this.state.messages.filter((message) => {
        return message.id !== id;
      }),
    });
    setTimeout(() => {
      this.changeHeader();
    }, 0);
  };

  editMessage = ({ id, text }) => {
    this.setState({
      messages: this.state.messages.map((message) => {
        if (message.id === id) {
          message.text = text;
          message.editedAt = new Date().toString();
        }
        return message;
      }),
      edtMsg: {
        id: "",
        editMsg: false,
        text: "",
      },
    });
  };

  clickToEdit = (id) => {
    const text = this.state.messages.filter((message) => message.id === id);
    this.setState({
      edtMsg: {
        id,
        editMsg: true,
        text,
      },
    });
  };

  render() {
    const data = this.state;
    return (
      <div className="pad-lr_chat">
        <Header roomInfo={data.roomInform} />
        <div className="chat-messageWndw marg-t_20px">
          {data.messages.length !== 0 ? (
            <MessageList
              messages={data.messages}
              myId={data.myInform.userId}
              deleteMessage={this.deleteMessage}
              clickToEdit={this.clickToEdit}
            />
          ) : (
            <Spiner />
          )}
        </div>
        {this.state.edtMsg.editMsg ? (
          <MessageInput
            edtMsg={this.state.edtMsg}
            editMessage={this.editMessage}
          />
        ) : (
          <MessageInput addMessage={this.addNewMessage} />
        )}
      </div>
    );
  }
}

export default Chat;
