import React from "react";
import Message from "./Message";
import Time from './Time';

function MessageList(props) {
  let timeNow = new Date();
  let prevDate = "";
  let time;
  return (
    <div className="full-size flex-drctCol">
      {
        props.messages.map((data) => {
          if(timeNow !== new Date(data.createdAt).toLocaleDateString()){
            time = <Time data={data} prevDate={prevDate} cls={"date-block"}/>
            prevDate = new Date(data.createdAt).toLocaleDateString();
          }
            return (
            <>
                {time}
                <Message
                    data={data}
                    myId={props.myId}
                    deleteMessage={props.deleteMessage}
                    clickToEdit={props.clickToEdit}
                    key={data.id}
                />
            </>
            );
        })
      }
    </div>
  );
}

export default MessageList;
