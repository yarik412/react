import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

class MessageInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msg:{
        id: "",
        text: "",
        createdAt: ""
      },
      editedMsg:{
        id: "",
        text: ""
      }
    };
  }
  inputHandlerNew = (e) => {
    const text = e.target.value.trim();
    const id = uuidv4();
    const date = new Date();
    this.setState({
      msg:{
        text:text,
        id: id,
        createdAt: date.toString(),
      }
    });
  };
  inputHandlerEdit = (e) =>{
    const text = e.target.value;
    this.setState({
      editedMsg:{
        id:this.props.edtMsg.id,
        text:text
      }
    });
  };

  sendText = () => {
    if(this.state.msg.text){
      this.props.addMessage(this.state.msg);
      this.state.msg.text = "";
    }
  };

  editText = () => {
    this.props.editMessage(this.state.editedMsg);
    this.state.editedMsg.text = "";
  };

  render() {
    return (
      <div>
        <div className="pad-lr_30px chat-msg_submit flex flex-vertCntr">
        {
          this.props.editMessage
          ?
            <>
              <input
                className="chat-msg_input"
                type="text"
                name="message"
                placeholder={this.props.edtMsg.text[0].text}
                onChange={(e) => this.inputHandlerEdit(e)}
              />
              <button className="chat-msg_button" onClick={this.editText}>
                Edit
              </button>
            </>
          :
          <>
            <input
              className="chat-msg_input"
              type="text"
              name="message"
              placeholder="Message"
              value={this.state.msg.text}
              onChange={(e) => this.inputHandlerNew(e)}
            />
            <button className="chat-msg_button" onClick={this.sendText}>
              Send
            </button>
          </>
        }
        </div>
      </div>
    );
  }
}

export default MessageInput;
