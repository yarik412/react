import React from "react";
import Avatar from "../helpers/Avatar";
import Like from "../helpers/Like";
import Delete from "../helpers/Delete";
import Edit from "../helpers/Edit";

function Message(props) {
  let [hour, minute] = "";
  let newMsg = true;
  if (props.data.editedAt) {
    [hour, minute] = new Date(props.data.editedAt)
      .toLocaleTimeString()
      .split(":");
    newMsg = false;
  } else {
    [hour, minute] = new Date(props.data.createdAt)
      .toLocaleTimeString()
      .split(":");
  }

  return (
    <div>
      {props.data.userId === props.myId ? (
        <div
          id={props.data.id}
          className=" flex flex-vertCntr flex-drctCol marg-t_20px pos_right pad-r_10px"
        >
          <div className="chat-msg chat-msg_rightDIV">
            <div className="chat-msg_right">
              <div className="font-mid">{props.data.text}</div>
              <div className="msg-date flex flex-spcbtwn">
                {newMsg ? `${hour}:${minute}` : `${hour}:${minute} (edited)`}
              </div>
            </div>
            <div className="flex flex-vertCntr flex-spcbtwn width_100prc msg-iconDiv marg-t_20px">
              <div className="like-right_size marg-l_20px">
                <Like />
              </div>
              <div className="flex flex-vertCntr flex-spcbtwn">
                <Edit func={() => props.clickToEdit(props.data.id)} />
                <Delete func={() => props.deleteMessage(props.data.id)} />
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className=" flex flex-vertCntr pos_left marg-t_20px pad-l_10px">
          <Avatar src={props.data.avatar} />
          <div className="chat-msg chat-msg_left">
            <div className="font-small">{props.data.user}</div>
            <div className="font-mid">{props.data.text}</div>
            <div className="msg-date">
              {newMsg ? `${hour}:${minute}` : `${hour}:${minute} (edited)`}
            </div>
            <div className="msg-iconDiv">
              <label>
                <input type="checkbox" className="likeDiv visible-hide" />
                <Like />
              </label>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Message;
